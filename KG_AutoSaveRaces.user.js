// ==UserScript==
// @name           KG_AutoSaveRaces
// @version        1.0.0
// @namespace      klavogonki
// @author         NIN
// @description    Автоматическое сохранение заездов
// @include        http*://klavogonki.ru/g/*
// @grant          none
// ==/UserScript==

(function() {

//https://www.geeksforgeeks.org/how-to-trigger-a-file-download-when-clicking-an-html-button-or-javascript/
function download(filename, text) {
	let element = document.createElement('a');
	element.setAttribute('href',
	    'data:text/plain;charset=utf-8, '
	    + encodeURIComponent(text));
	element.setAttribute('download', filename);

	document.body.appendChild(element);
	element.click();

	document.body.removeChild(element);
}

function downloadPage() {
	const text = document.documentElement.outerHTML;
	const filename = (new Date()).toISOString() + ".html";
	download(filename, text);
}

let firstunload = true;

window.addEventListener('beforeunload', (event) => {
	if (firstunload) {
		firstunload = false;
		setTimeout(downloadPage, 0);
	}
});

})();
