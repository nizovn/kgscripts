// ==UserScript==
// @name           KG_ClubTestCars
// @version        1.0.0
// @namespace      klavogonki
// @author         NIN
// @description    Клуб тестирования машинок
// @include        http*://klavogonki.ru/g/*
// @include        http*://klavogonki.ru/gamelist*
// @include        http*://klavogonki.ru/top*
// @include        http*://klavogonki.ru/vocs/*/top*
// @include        http*://klavogonki.ru/u/*
// @grant          none
// ==/UserScript==

(function() {

let database = {};
let database_cars = {};
let updated = undefined;
const defaultSettings = {
	database: {},
	database_cars: {},
	updated: undefined
};
var callback_func = undefined;
{
	let data;
	try {
		data = JSON.parse(localStorage.clubTestCars);
	}
	catch {
		data = defaultSettings;
	}
	if (typeof(data)!=="object") {
		data = defaultSettings;
	}
	if (data.hasOwnProperty("database") && (typeof(data.database)==="object")) {
		database = data.database;
	} else {
		data.database = defaultSettings.database;
	}
	if (data.hasOwnProperty("database_cars") && (typeof(data.database_cars)==="object")) {
		database_cars = data.database_cars;
	} else {
		data.database_cars = defaultSettings.database_cars;
	}
	if (data.hasOwnProperty("updated")) {
		updated = data.updated;
	} else {
		data.updated = defaultSettings.updated;
	}
	localStorage.clubTestCars = JSON.stringify(data);
}

function observer_callback(mutationsList, observer) {
	for (let mutation of mutationsList) {
		if (mutation.type === 'childList') {
			observer.disconnect();
			callback_func();
			observer.observe(targetNode, config);
		}
	}
}

function replace_car(id, car) {
	if (!database.hasOwnProperty(id)) return false;
	const newcar = car.innerHTML.replace(/car\d+(-add)?/g, "car_club"+database[id]);
	if (newcar !== car.innerHTML) {
		car.innerHTML = newcar;
		return true;
	}
	return false;
}

function gamelist() {
	const regexp_id = /\/profile\/(\d+)\/?.*/;
	let cars = $$(".car");
	for (let car of cars) {
		let p = car.parentElement.getElementsByClassName("name")[0].getElementsByTagName('a');
		if (p.length < 1) continue;
		p = p[0].pathname;
		const found = p.match(regexp_id);
		if (!found || found.length != 2) continue;
		const id = found[1];
		replace_car(id, car);
	}
}

function playerslist() {
	const regexp_id = /\/profile\/(\d+)\/?.*/;
	let cars = $$("table.car");
	for (let car of cars) {
		let p = car.getElementsByClassName("name")[0].getElementsByTagName('a');
		if (p.length < 1) continue;
		p = p[0].pathname;
		const found = p.match(regexp_id);
		if (!found || found.length != 2) continue;
		const id = found[1];
		replace_car(id, car);
	}
}

function profile_callback(mutationsList, observer) {
	for(let mutation of mutationsList) {
		if (mutation.type === 'childList') {
			let cars = $$(".profile-header .car");
			for (let car of cars) {
				if (replace_car(id, car)) {
					observer.disconnect();
				}
			}
		}
	}
}

let r_id;
if (location.href.match(/^https?:\/\/klavogonki\.ru\/gamelist/)
    && document.getElementById("gamelist") != undefined) {
	var targetNode = document.getElementById("gamelist");
	var config = { attributes: true, childList: true, subtree: true };
	callback_func = gamelist;
	let observer = new MutationObserver(observer_callback);
	observer.observe(targetNode, config);
}
else
if (location.href.match(/^https?:\/\/klavogonki\.ru\/top\//)) {
	gamelist();
}
else
if (location.href.match(/^https?:\/\/klavogonki\.ru\/vocs\/\d+\/top\//)) {
	gamelist();
}
else
if (r_id = location.href.match(/^https?:\/\/klavogonki\.ru\/u\/#\/(\d+)/)) {
	var targetNode = document.getElementById("content");
	var config = { attributes: true, childList: true, subtree: true };
	var id = r_id[1];
	let observer = new MutationObserver(profile_callback);
	observer.observe(targetNode, config);
}
else
if (location.href.match(/^https?:\/\/(test\.)?klavogonki\.ru\/g\//)) {
	var targetNode = document.getElementById("players");
	var config = { attributes: true, childList: true, subtree: true };
	callback_func = playerslist;
	let observer = new MutationObserver(observer_callback);
	observer.observe(targetNode, config);
}
else {
	return;
}

var inject_css = document.createElement("style");
inject_css.setAttribute("type", "text/css");
inject_css.innerHTML = genCSS();
document.body.appendChild(inject_css);

const sheet_members_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQutSrt0IDHnNagb7IzC6_NpNqfkh7ztnMWlktNMOtRawDxNBhtidNQrdXqSNEMY4dKYSUX-iGsHmY_/pub?gid=0&single=true&output=csv";
const sheet_cars_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQutSrt0IDHnNagb7IzC6_NpNqfkh7ztnMWlktNMOtRawDxNBhtidNQrdXqSNEMY4dKYSUX-iGsHmY_/pub?gid=1680852306&single=true&output=csv";

function addToDatabase(_database, id, car) {
	if (_database.hasOwnProperty(id)) return;
	_database[id] = car;
}

function genCSS() {
	let css = "";
	for (let id in database_cars) {
		css += '.car .car_club'+id+' { background: white url("'+database_cars[id]+'") no-repeat; }\n';
		css += '.car_club'+id+'.car-tuning {display:none;}\n';
	}
	return css;
}

async function updateDatabase() {
	let database_tmp = {};
	let database_cars_tmp = {};
	try {
		let response = await fetch(sheet_members_url);
		let data = await response.text();
		data = data.replace(/\r\n/g,"\n");
		let lines = data.split("\n");
		for (let line of lines) {
			let l = line.split(",");
			if (l.length<2)
				continue;
			let id = l[0];
			if (id.length==0 || isNaN(id))
				continue;
			let car = l[1];
			if (car.length==0 || isNaN(car))
				continue;
			addToDatabase(database_tmp, id, car);
		}
		response = await fetch(sheet_cars_url);
		data = await response.text();
		data = data.replace(/\r\n/g,"\n");
		lines = data.split("\n");
		for (let line of lines) {
			let l = line.split(",");
			if (l.length<3)
				continue;
			let id = l[0];
			if (id.length==0 || isNaN(id))
				continue;
			let car = l[1];
			if (car.length==0)
				continue;
			let enabled = l[2];
			if (enabled!="TRUE")
				continue;
			addToDatabase(database_cars_tmp, id, car);
		}
		database = database_tmp;
		database_cars = database_cars_tmp;
		localStorage.clubTestCars = JSON.stringify({
			"database":database,
			"database_cars":database_cars,
			"updated":new Date()
			});
	}
	catch(error) {
		console.log("error: "+error);
	}
}

seconds_passed = (new Date() - new Date(updated?updated:0))/1000;
if (seconds_passed > 60*10)
	updateDatabase();

})();

