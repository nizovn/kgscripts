// ==UserScript==
// @name           KG_HighQualityCars
// @version        1.0.2
// @namespace      klavogonki
// @author         NIN
// @description    Тестирование качественных машинок
// @include        http*://klavogonki.ru/g/*
// @include        http*://klavogonki.ru/gamelist*
// @include        http*://klavogonki.ru/top*
// @include        http*://klavogonki.ru/vocs/*/top*
// @include        http*://klavogonki.ru/u/*
// @grant          none
// ==/UserScript==

(function() {

let database_cars = {};
let updated = undefined;
const defaultSettings = {
	database_cars: {},
	updated: undefined
};

var callback_func = undefined;
{
	let data;
	try {
		data = JSON.parse(localStorage.KG_HighQualityCars);
	}
	catch (err) {
		data = defaultSettings;
	}
	if (typeof(data)!=="object") {
		data = defaultSettings;
	}

	if (data.hasOwnProperty("database_cars") && (typeof(data.database_cars)==="object")) {
		database_cars = data.database_cars;
	} else {
		data.database_cars = defaultSettings.database_cars;
	}
	if (data.hasOwnProperty("updated")) {
		updated = data.updated;
	} else {
		data.updated = defaultSettings.updated;
	}
	localStorage.KG_HighQualityCars = JSON.stringify(data);
}

function observer_callback(mutationsList, observer) {
	for (let mutation of mutationsList) {
		if (mutation.type === 'childList') {
			observer.disconnect();
			callback_func();
			observer.observe(targetNode, config);
		}
	}
}

const rand_corner = Math.floor(Math.random()*2+1);

function replacer(match, p1) {
	const i = parseInt(p1);
	if (i >= current_tuning.length) return match;
	const ind = (current_tuning[i]>0)?current_tuning[i]-1:"";
	return "car-tuning"+p1+"_"+ind;
}

function replace_car(id, car) {
	if (current_type == "all_cars") {
		current_tuning = random_tuning();
	} else {
		if (user_id != id) return false;
	}

	if (!database_cars.hasOwnProperty(current_car)) return false;
	let newcar = car.innerHTML.replace(/car\d+(-add)?/g, "test_corner"+rand_corner+" car_test"+current_car);

	const ind = (current_tuning.length>0 && current_tuning[0]>0)?current_tuning[0]-1:"";
	newcar = newcar.replace(/car\-base\_\d*/g, "car-base_"+ind);
	newcar = newcar.replace(/car\-tuning(\d*)\_\d*/g, replacer);

	if (newcar !== car.innerHTML) {
		car.innerHTML = newcar;
		return true;
	}
	return false;
}

function gamelist() {
	const regexp_id = /\/profile\/(\d+)\/?.*/;
	let cars = $$(".car");
	for (let car of cars) {
		let p = car.parentElement.getElementsByClassName("name")[0].getElementsByTagName('a');
		if (p.length < 1) continue;
		p = p[0].pathname;
		const found = p.match(regexp_id);
		if (!found || found.length != 2) continue;
		const id = found[1];
		replace_car(id, car);
	}
}

function playerslist() {
	const regexp_id = /\/profile\/(\d+)\/?.*/;
	let cars = $$("table.car");
	for (let car of cars) {
		let p = car.getElementsByClassName("name")[0].getElementsByTagName('a');
		if (p.length < 1) continue;
		p = p[0].pathname;
		const found = p.match(regexp_id);
		if (!found || found.length != 2) continue;
		const id = found[1];
		replace_car(id, car);
	}
}

function profile_callback(mutationsList, observer) {
	for(let mutation of mutationsList) {
		if (mutation.type === 'childList') {
			let cars = $$(".profile-header .car");
			for (let car of cars) {
				if (replace_car(id, car)) {
					observer.disconnect();
				}
			}
		}
	}
}

function random_car()
{
	const keys = Object.keys(database_cars);
	if (keys.length<=0) return "0";
	return keys[Math.floor(Math.random() * keys.length)].toString();
}

function random_tuning()
{
	let d = database_cars[current_car];
	if (typeof(d)!="object" || !d.hasOwnProperty("tuning")) return [];
	d = d.tuning;
	let res = [];
	for (let i=0;i<d.length;i++) {
		let dd = d[i];
		if (i>0) dd+=1;
		res.push(Math.floor(Math.random()*dd));
	}
	return res;
}

// start
const user_id = __user__;
const current_car = random_car();
//const current_car = "100";
let current_tuning = random_tuning();

const current_type = "usual";
// usual - заменяется только своя машинка
// all_cars - заменяются все машинки на current_car, с произвольно меняющимся тюнингом

let r_id;
var targetNode;
var config;
if (location.href.match(/^https?:\/\/klavogonki\.ru\/gamelist/)
    && document.getElementById("gamelist") != undefined) {
	targetNode = document.getElementById("gamelist");
	config = { attributes: true, childList: true, subtree: true };
	callback_func = gamelist;
	let observer = new MutationObserver(observer_callback);
	observer.observe(targetNode, config);
}
else
if (location.href.match(/^https?:\/\/klavogonki\.ru\/top\//)) {
	gamelist();
}
else
if (location.href.match(/^https?:\/\/klavogonki\.ru\/vocs\/\d+\/top\//)) {
	gamelist();
}
else
if (r_id = location.href.match(/^https?:\/\/klavogonki\.ru\/u\/#\/(\d+)/)) {
	targetNode = document.getElementById("content");
	config = { attributes: true, childList: true, subtree: true };
	var id = r_id[1];
	let observer = new MutationObserver(profile_callback);
	observer.observe(targetNode, config);
}
else
if (location.href.match(/^https?:\/\/(test\.)?klavogonki\.ru\/g\//)) {
	targetNode = document.getElementById("players");
	config = { attributes: true, childList: true, subtree: true };
	callback_func = playerslist;
	let observer = new MutationObserver(observer_callback);
	observer.observe(targetNode, config);
}
else {
	return;
}

const sheet_cars_url = "https://docs.google.com/spreadsheets/d/1Ec0HrM6ORNtww6Q2GvH6FvIN0OI3G7V08uWExlmTh6I/gviz/tq?tqx=out:csv&sheet=Cars"

function addToDatabase(_database, id, obj) {
	if (_database.hasOwnProperty(id)) return;
	_database[id] = obj;
}

function genWatermark(id, options) {
	let res = ''+
	'.car [class*="car-base"].test_corner'+id+'::before {'+
		'content:"TEST";'+
		'font-size:10px;'+
		'position: absolute;'+
		'top: 0px;'+
		'min-width: 100px;'+
		'background: #ffff00;'+
		'z-index: 100;'+
		'text-align: center;'+
		'color: #000;';
	for (const opt of options) {
		res += opt;
	}
	res += '}\n';
	return res;
}

function genTuning(id) {
	let res = '';
	if (!database_cars.hasOwnProperty(id)) return;
	let d = database_cars[id];
	if (typeof(d)!="object" || !d.hasOwnProperty("tuning")) return;
	d = d.tuning;
	if (d.length<1) return "";
	{
		const num_tun = d[0]-1;
		for (let j=0;j<num_tun;j++) {
			res += '.car_test'+id+'.car-base_'+j+' { background-position:'+(-100*(j+1))+'px 0px;}';
		}
	}
	for (let i=1;i<d.length;i++) {
		const num_tun = d[i];
		for (let j=0;j<num_tun;j++) {
			res += '.car_test'+id+'.car-tuning'+i+'_'+j+' { display:block; background-color: transparent;}';
		}
	}
	return res;
}

function genAllTuning() {
	let res = "";
	for (let id in database_cars) {
		res += genTuning(id);
	}
	return res;
}

function genCSS() {
	let css = "";
	for (let id in database_cars) {
		let d = database_cars[id];
		let car_url = "";
		if (typeof(d)==="object" && d.hasOwnProperty("url"))
			car_url = d.url;
		else
			if (typeof(d)==="string")
				car_url = d;
		else
			continue;
		css += '.car .car_test'+id+' { background-color: white;background-image: url("'+car_url+'");background-repeat-x:no-repeat;background-repeat-y:no-repeat; }\n';
	}
	css += '.car-tuning[class*="car_test"] {display:none;}\n';
	css += '[class*="car_test"][class*="car-base_"] {background-position:0px 0px;}\n';
	css += genWatermark(1,['left:0px;','transform: translateX(-50%) translateY(-50%) rotate(-45deg) translateY(16px);']);
	css += genWatermark(2,['right:0px;','transform: translateX(50%) translateY(-50%) rotate(45deg) translateY(16px);']);
	return css;
}

var inject_css = document.createElement("style");
inject_css.setAttribute("type", "text/css");
inject_css.innerHTML = genCSS() + genAllTuning();
document.body.appendChild(inject_css);

async function updateDatabase() {
	let database_cars_tmp = {};
	try {
		let response = await fetch(sheet_cars_url);
		let data = await response.text();
		data = data.replace(/\r\n/g,"\n");
		data = data.replace(/"/g,"");
		let lines = data.split("\n");
		for (let line of lines) {
			let l = line.split(",");
			if (l.length<3) continue;
			let id = l[0];
			if (id.length==0 || isNaN(id)) continue;
			let car = l[1];
			if (car.length==0) continue;
			let enabled = l[2];
			if (enabled!="TRUE") continue;

			let tuning = [];
			if (l.length>10 && l[10].length>0) {
				let tmp_tuning = l[10].split(";");
				for (let t of tmp_tuning) {
					const n = isNaN(t)?0:parseInt(t);
					tuning.push(n);
				}
			}

			addToDatabase(database_cars_tmp, id,{"url":car,"tuning":tuning});
		}
		database_cars = database_cars_tmp;
		localStorage.KG_HighQualityCars = JSON.stringify({
			"database_cars":database_cars,
			"updated":new Date()
			});
	}
	catch(error) {
		console.log("error: "+error);
	}
}

const seconds_passed = (new Date() - new Date(updated?updated:0))/1000;
if (seconds_passed > 60*10) {
	updateDatabase();
}

})();

