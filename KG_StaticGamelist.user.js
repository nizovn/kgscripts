// ==UserScript==
// @name           KG_StaticGamelist
// @version        1.3.0
// @namespace      klavogonki
// @author         NIN
// @description    Интерфейс списка заездов, гарантирующий статичное положение заезда на странице
// @include        http*://klavogonki.ru/gamelist*
// @exclude        http*://klavogonki.ru/gamelist?*
// @grant          none
// ==/UserScript==

// Почти полностью основано на старом варианте списка заездов (gamelist.js)

function main() {
let obj_allowedGames = {};
let obj_allowedGames_prev = {};
const num_allowedGames = 5;

var tplListitem = new Template('\
<tr class=item id=ngame#{id}>\
<td class=enter>#{enter_html}</td>\
<td class=sign>#{sign_html}</td>\
<td><div class=status>#{html_status}</div></td>\
<td class=players>\
<div class=playerscontainer>\
#{players_html}\
</div>\
</td>\
</tr>');

var tplStatus = {
	time: new Template('<div class=remain><span id=remain#{id}>#{remain}</span></div>#{html_custominfo}'),
	paused: new Template('<div class=gray_timer><span>#{remain}</span></div>#{html_custominfo}'),
	active: new Template('Идет игра#{html_custominfo}')
};

var tplCustomInfo = new Template('<div class=custominfo>Таймаут:&nbsp;#{timeout}<br>#{levels}</div>');

var tplAnonymPlayer = new Template('\
<td class="other" id=player#{id}>\
<div class=car><div class=imgcont id=imgcont#{id} style="background-color:#777;#{opacity}">\
<div class="img car1" style="background-color:transparent"></div>\
</div></div>\
<div class=name id=player_name#{id} class="#{rang} #{status}" style="#{opacity_name}">#{name}</div>\
</td>\
');

var tplUserPlayer = new Template('\
<td class="other" id=player#{id}>\
<div class=car>\
<div class=imgcont id=imgcont#{id} style="background: #{user.background}; #{opacity}">\
<div class="img car#{user.car} car-base_#{user.tuning[0]}" style="background-color: transparent; color: #{user.color}"></div>\
<div class="img car#{user.car} car-tuning car-tuning1_#{user.tuning[1]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning2_#{user.tuning[2]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning3_#{user.tuning[3]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning4_#{user.tuning[4]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning5_#{user.tuning[5]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning6_#{user.tuning[6]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning7_#{user.tuning[7]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning8_#{user.tuning[8]}"></div>\
<div class="img car#{user.car} car-tuning car-tuning9_#{user.tuning[9]}"></div>\
</div>\
#{rating_html}\
</div>\
<div class=name id=player_name#{id} style="#{opacity_name}; #{padding_name}">\
#{avatar_html}\
<a href="/profile/#{user.id}/" class="#{rang} #{status} profile" onmousemove="showProfile(#{user.id});" onmouseover="showProfile(#{user.id});" onmouseout="$(\'popup\').hide();">\
#{name}\
</a>\
</div>\
</td>');

var tplAnonymReducedPlayer = new Template('\
		<div class=name id=player_name#{id} class="#{rang} #{status}" style="#{opacity_name}">#{name}</div>');

var tplUserReducedPlayer = new Template('\
		<div class=name id=player_name#{id} style="#{opacity_name}; #{padding_name}">\
		#{avatar_html}\
		<a href="/profile/#{user.id}/" class="#{rang} #{status} profile" onmousemove="showProfile(#{user.id});" onmouseover="showProfile(#{user.id});" onmouseout="$(\'popup\').hide();">\
		#{name}\
		</a>\
		</div>');

var tplRatingSignPlayer = new Template('<div class=car_rating style="color: #{color};"><span class=bitless2>№</span>#{rating7}</div>');

var tplEnter = new Template('<div id=enter#{id} class=enter><a class="#{competition_html}" href="/g/?gmid=#{id}" onclick="if(typeof chatLeaveRoom != "undefined") chatLeaveRoom(\'general\');" title="Войти в игру">&nbsp;#{regular_rate_html}</a></div>');
var tplEnterDisabled = new Template('<div class="enter"><a class="#{competition_html} notallowed">&nbsp;</a>#{regular_rate_html}</div>');
	
var prevhash = new Object();

var cached_users = new Object();

var dataRequest = {_complete: true};

var chat = null;

var Listitem = Class.create
({
	initialize: function(info,time)
	{
		this.update(info,time);
	},
	update: function(info,time)
	{
		var _info = info;
		info = _info.info;
		info.players = _info.players;
		info.allowed = true;

		this.info = info;
		
		var tpl = tplListitem;

		info.html_custominfo = '';
		info.remain = '00:30';
		if(info.custom)
		{
		
			info.html_custominfo = '<div class=custominfo>';
			var custominfo = new Array();
			
			if(info.params.level_from>1 || info.params.level_to<9)
			{			
				var level_hash = new Array('Новички','Любители','Таксисты','Профи','Гонщики','Маньяки','Супермены','Кибергонщики','Экстракиберы');
				if(info.params.level_from==info.params.level_to)
					custominfo.push(level_hash[info.params.level_from-1]);
				else
					custominfo.push(level_hash[info.params.level_from-1]+'&nbsp;&mdash;&nbsp;'+level_hash[info.params.level_to-1]);
			}		
			
			var gametype_titles = {normal: 'Обычный', sprint: 'Спринт', marathon: 'Марафон', noerror: 'Безошибочный', abra: 'Абракадабра', digits: 'Цифры', chars: 'Буквы', referats: 'Яндекс.Рефераты'};
							
            var str_qual = '', str_qual_title = '';
            if(info.params.qual) {
                str_qual = '<i class="gametype-sign qual"></i>';
                str_qual_title = ' - квалификация';
            }
			info.sign_html = '<i class="gametype-sign active sign-'+info.params.gametype+'" title="'+gametype_titles[info.params.gametype]+str_qual_title+'">'+str_qual+'</i>';
			if(/voc-\d+/.test(info.params.gametype)) {
				info.sign_html = '<i class="gametype-sign active sign-voc" title="По словарю'+str_qual_title+'">'+str_qual+'</i>';
				if(info.params.voc.name.length > 25)
					info.params.voc.name = info.params.voc.name.substr(0,23)+'...';
				custominfo.push('<span class=gametype-voc><span style="white-space:nowrap;">&laquo;<a href="/vocs/'+info.params.voc.id+'/">'+info.params.voc.name+'</a>&raquo;</span></span>');
			}
			
			var regular_competition_html = '';
			if(info.params.competition)
				regular_competition_html = _tpl('competition',info);
			
			info.html_custominfo = '<div class=custominfo>'+custominfo.join("<br>")+regular_competition_html+'</div>';		

			
			if(!info.begintime)
			{
				var timeout_hash = {
					t10: '00:10',
					t20: '00:20',
					t30: '00:30',
					t45: '00:45',
					t60: '01:00',
					t120: '02:00',
					t180: '03:00'};			
				info.remain = timeout_hash['t'+info.params.timeout];
			}
		}
		else {
			info.sign_html = '<i class="gametype-sign active sign-'+'normal'+'" title="'+'Обычный'+'">'+'</i>';
		}
		
		
		
		
		this.begintime = 0;		

		if(info.params && info.params.competition)			
		{
			info.competition_html = 'competition';
		}
		info.enter_html = tplEnterDisabled.evaluate(info); 	
		if(true)
		{			
			if(info.begintime)
			{
				/*
				var now = new Date();
				this.begintime = info.begintime - time + now.getTime() / 1000;
				var timeout = info.begintime-time;
				info.remain = ((timeout/60).format()+':'+(timeout%60).format());
				*/
				info.remain = scope.getRemainTime(_info).label;
				if(info.allowed)
					info.enter_html = tplEnter.evaluate(info); 	
				info.html_status = tplStatus.time.evaluate(info);
			}
			else
			{
				if(info.allowed)
					info.enter_html = tplEnter.evaluate(info);
				
				info.html_status = tplStatus.paused.evaluate(info);
			}
		}
		else
		{
			info.html_status = tplStatus.active.evaluate(info);
		}
		
		info.players_html = new String();
		var max_players_width = 4 + Math.floor( ( $('content').getWidth() - 850 ) / 120 );
		if(max_players_width < 4)
			max_players_width = 4;
		let cont_elem = $$('.playerscontainer')[0];
		if (cont_elem)
			max_players_width = Math.max(1, Math.floor(cont_elem.getWidth() / 120));
		else
			max_players_width = 4;
		
		var reduced_display = info.players.length > max_players_width;
		if(reduced_display)
			var players_html_arr = new Array();
		else
			info.players_html = '<table class=normal-display><tr>';
		for(var i=0;i<info.players.length;i++)
		{
			if(info.players[i] == null)
				continue;
			if(info.players[i].user != null)
			{
				var player_data = clone(info.players[i]);
				/*
				if(cached_users[info.players[i].user] == undefined)
				{
					cached_users[info.players[i].user] = clone(info.players[i]);					
				} 
				
				var player_data = clone(cached_users[info.players[i].user]);
				
				player_data.id = info.players[i].id;
				player_data.leave = info.players[i].leave;
				player_data.rating7 = info.players[i].rating7;				
				*/
			}
			else
				var player_data = info.players[i];
						
			if(player_data.avatar)
			{
				player_data.avatar_html = '<i style="background: white url('+player_data.avatar+') no-repeat 0% 50%;"></i>';
				player_data.padding_name = 'padding: 0 20px;';
			}
			
			if(!__user_prefs.no_colored_rangs) {
				player_data.rang = 'rang'+player_data.level;
				player_data.status = 'status'+player_data.status;
			}
				
			
			if(reduced_display)
			{
				var tpl_player = player_data.user ? tplUserReducedPlayer : tplAnonymReducedPlayer;
				players_html_arr.push(tpl_player.evaluate(player_data));
			}
			else
			{
				player_data.color = '#000000';
				if(/#[a-fA-F0-9]+/.test(player_data.background))
					player_data.color = colortools.capByBrightness(player_data.background);
			
				if(player_data.rating7)
					player_data.rating_html = tplRatingSignPlayer.evaluate(player_data);
	
				
				if(player_data.leave)
				{
					if(!Prototype.Browser.IE) 
						player_data.opacity_name = 'opacity: 0.3';		
					if(Prototype.Browser.Opera)
						player_data.background = 'white';		
					player_data.opacity = 'opacity: 0.3';
				}
				
				
			
				var tpl_players = tplAnonymPlayer;
				if(player_data.user)
					tpl_players = tplUserPlayer;	
				
				info.players_html += tpl_players.evaluate(player_data);
			}
		}
		
		if(reduced_display)
		{
			var rows = Math.ceil(info.players.length / (__vk ? 3 : 6));
			rows = Math.ceil(info.players.length / max_players_width);
			if(rows < 3)
				rows = 3;
			var cols = Math.ceil(info.players.length / rows);
			if(__vk && cols > 3)
			{
				cols = 3;
				rows = Math.ceil(info.players.length / cols);
			}
			info.players_html += '<table class=reduced-display>';
			for(var iRow=0; iRow < rows; iRow++)
			{
				info.players_html += '<tr>';
				for(var iCol=0; iCol < cols; iCol++)
				{
					info.players_html += '<td>';
					var idx = iRow + iCol*rows; 					
					info.players_html += idx < players_html_arr.length ? players_html_arr[idx] : '&nbsp;';
					info.players_html += '</td>';					
				}
				info.players_html += '</tr>';
			}
			info.players_html += '</table>';
		}
		else
			info.players_html += '</tr></table>';
				
		var html = tpl.evaluate(info);

		saveHTML(info.id, html);

		/*
		if($('game'+info.id))
		{
			if($('game'+info.id).prev == info.prev)
			{
				$('game'+info.id).replace(html);
			}
			else
			{
				$('game'+info.id).remove();
				if($('game'+info.prev))		
					$('game'+info.prev).insert({after:html});
				else
					dbg('Prev game container "game'+info.prev+'" not found.');
			}
		}
		else
		{
			if($('game'+info.prev))		
				$('game'+info.prev).insert({after:html});
			else
				dbg('Prev game container "game'+info.prev+'" not found.');
		}
		
		if($('game'+info.id))		
			$('game'+info.id).prev = info.prev;
		else
			dbg('Game container "game'+info.id+'" not found.');
		*/
		
		for(var i=0;i<info.players.length;i++)
		{
			if(info.players[i] == null)
				continue;
			if(info.players[i].leave && !reduced_display)
			{				
				var obj = $('imgcont'+info.players[i].id);
				if(obj != null)
					obj.setOpacity(0.3);
      			if(!Prototype.Browser.IE) {
					var obj = $('player_name'+info.players[i].id);
					if(obj != null)
						obj.setOpacity(0.3);
				}
			}
		}
	}
});


var gamelist = new Object();

var visualGamelistSize = 10;
var visualGamelist = [];

function initGamelist() {
	visualGamelistSize = 10;
	visualGamelist = [];
	for (var i=0;i<visualGamelistSize;i++)
		visualGamelist.push({id:0,time:0,scrollPos:{}});
}
initGamelist();

function getEmptyIndex(id) {
	// 0 - для fixed
	for (var i=1;i<visualGamelistSize;i++) {
		if (visualGamelist[i].id == id)
			return i;
	}
	for (var i=1;i<visualGamelistSize;i++) {
		if ((visualGamelist[i].id == 0) && (visualGamelist[i].time == 0))
			return i;
	}
	visualGamelistSize += 1;
	visualGamelist.push({id:0,time:0,scrollPos:{}});
	return visualGamelistSize-1;
}

var htmlList = {};
function saveHTML(id, html) {
	htmlList[id] = html;
}

var placeholder = tplListitem.evaluate();
placeholder = placeholder.replace(' id=game>','>');
var seconds_reserved = 3;
var scope = undefined;
var st_columns = 3;
var hide_guests = false;

const defaultTimeout = 3;
const defaultSettings = {
	timeout: false,
	hide_guests: false,
	columns: 1
};
{
	let data;
	try {
		data = JSON.parse(localStorage.staticGamelist);
	}
	catch {
		data = defaultSettings;
	}
	if (typeof(data)!=="object") {
		data = defaultSettings;
	}
	if (data.hasOwnProperty("timeout") && (typeof(data.timeout)==="boolean")) {
		seconds_reserved = (data.timeout)? defaultTimeout : 0;
	} else {
		data.timeout = false;
		seconds_reserved = 0;
	}
	if (data.hasOwnProperty("columns") && (typeof(data.columns)==="number")) {
		st_columns = (data.columns>0)? Math.trunc(data.columns) : defaultSettings.columns;
	} else {
		data.columns = defaultSettings.columns;
		st_columns = defaultSettings.columns;
	}
	if (data.hasOwnProperty("hide_guests") && (typeof(data.hide_guests)==="boolean")) {
		hide_guests = data.hide_guests;
	} else {
		data.hide_guests = false;
	}
	localStorage.staticGamelist = JSON.stringify(data);
}

function createTables()
{
	let _tc = document.getElementById("tableContainer");
	let n_childs = _tc.children.length;
	if (st_columns !== n_childs) {
		initGamelist();
	}
	if (st_columns < n_childs) {
		for (var i=0;i<n_childs-st_columns;i++) {
			const id = "staticTable"+(i+st_columns);
			let _t = document.getElementById(id);
			if (_t)
				_t.outerHTML = "";
		}
	}
	for (var i=0;i<st_columns;i++) {
		const id = "staticTable"+i;
		let _t = document.getElementById(id);
		if (!_t) {
			_t = document.createElement("table");
			_t.id = id;
			_tc.append(_t);
		}
		_t.width = 100./st_columns+"%";
	}
	const _scale = getScale();
	if (_scale < 1) {
		_tc.style.transform = "scale("+_scale+")";
		_tc.style.transformOrigin = "0 0";
		_tc.style.width = 100/_scale+"%";
	}
	else {
		_tc.style.transform = "";
		_tc.style.transformOrigin = "";
		_tc.style.width = "100%";
	}
}

function getScale()
{
	const container_w = (75+53+150+130)*st_columns;
	const _scale = jQuery("#tableContainer").parent().width() / container_w;
	return _scale;
}

function checkGuests(game) {
	let _info = JSON.parse(JSON.stringify(game.info));
	if (_info.hasOwnProperty("players")) {
		let _players = _info.players;
		let all_guests = true;
		for (let p of _players) {
			if (p && (p.user!==null)) {

				if (p.user["id"] !== undefined) {
					let user_id = p.user.id;
					if (obj_allowedGames[user_id] === undefined)
						obj_allowedGames[user_id] = [];

					if (obj_allowedGames_prev.hasOwnProperty(user_id)) {
						if (obj_allowedGames_prev[user_id].includes(game.id)) {
							obj_allowedGames[user_id].push(game.id);
						}
						else if ([...new Set(obj_allowedGames_prev[user_id].concat(obj_allowedGames[user_id]))].length < num_allowedGames) {
							obj_allowedGames[user_id].push(game.id);
						}
					}
					else
						if (obj_allowedGames[user_id].length < num_allowedGames) {
							obj_allowedGames[user_id].push(game.id);
						}

					if (!obj_allowedGames[user_id].includes(game.id))
						continue;
				}

				all_guests = false;

			}
		}
		if (all_guests) {
			return true;
		}
	}
	return false;
}

function updateList()
{
	createTables();
	var scrollPos = [];
	var oldSize = visualGamelistSize;
	for (var i=0;i<visualGamelistSize;i++) {
		const _i_column = i % st_columns;
		const _i_row = Math.trunc(i/st_columns);
		let row = $('staticTable'+_i_column).rows[_i_row];
		if (row == undefined)
			continue;
		var content = row.querySelector('.playerscontainer');
		if (content == undefined)
			continue;
		visualGamelist[i].scrollPos = {top:content.scrollTop,left:content.scrollLeft};
	}

	var games_cnt=0;
	var json_has_game = new Object();
	if (scope == undefined)
		scope = angular.element($("gamelist")).scope();
	if (scope == undefined)
		return;
	var gamelistOpen = scope.gamelists.open;
	var gamelistFixed = scope.gamelists.fixed;

	var json = {};
	var now = new Date();
	var now_time = now.getTime() / 1000;
	json.time = now_time;

	for(let glist of [gamelistOpen, gamelistFixed]) {
		json.gamelist = glist;
		for(var i=0;i<json.gamelist.length;i++)
		{		
			games_cnt++;
			if(!gamelist[json.gamelist[i].id])
				gamelist[json.gamelist[i].id] = new Listitem(json.gamelist[i],json.time);
			else
				gamelist[json.gamelist[i].id].update(json.gamelist[i],json.time);
			
			json_has_game[json.gamelist[i].id] = true;
		}
	}
	for(var id in gamelist)
	{	
		if(!/^\d+$/.test(id))
			continue;
			
		if(!json_has_game[id])
		{
			gamelist[id] = undefined;
			if($('ngame'+id))
				$('ngame'+id).remove();
		}
	}

	var newList = [];
	let gcount = 0;
	obj_allowedGames = {};
	for (var id in gamelistOpen) {
		if("function" != typeof gamelistOpen[id]) {
			var game = gamelistOpen[id];
			if (checkGuests(game)) {
				gcount += 1;
				if (hide_guests)
					continue;
			}
			var freeidx = getEmptyIndex(game.id);
			visualGamelist[freeidx].id = game.id;
			visualGamelist[freeidx].time = now_time;
			newList.push(game.id);
		}
	}
	obj_allowedGames_prev = obj_allowedGames;

	if(gamelistFixed[0] != undefined) {
		var game = gamelistFixed[0];
		visualGamelist[0].id = game.id;
		visualGamelist[0].time = now_time;
		newList.push(game.id);
	}

	for (var i=0;i<visualGamelistSize;i++) {
		if (newList.indexOf(visualGamelist[i].id) < 0) {
			visualGamelist[i].id = 0;
			if (now_time - visualGamelist[i].time > seconds_reserved) {
				visualGamelist[i].time = 0;
			}
		}
	}

	for (var i=0;i<st_columns;i++) {
		$('staticTable'+i).innerHTML = genTable(i, visualGamelistSize, st_columns);
	}
	document.getElementById("guests_count").innerHTML = gcount;

	// выставление высоты контейнера после масштабирования
	let _tc = document.getElementById("tableContainer");
	const _scale = getScale();
	if (_scale < 1) {
		_tc.style.marginBottom = -_tc.getHeight()*(1-_scale)+"px";
	} else {
		_tc.style.marginBottom = "";
	}

	for (var i=0;i<visualGamelistSize;i++) {
		const _i_column = i % st_columns;
		const _i_row = Math.trunc(i/st_columns);
		let row = $('staticTable'+_i_column).rows[_i_row];
		if (row == undefined)
			continue;
		let content = row.querySelector('.playerscontainer');
		if (content == undefined)
			continue;
		if (content.firstChild && content.firstChild.hasClassName("normal-display")) {
			content.style.overflow = "visible";
		}
		if ((content.clientWidth < content.scrollWidth)
		 || (content.clientHeight < content.scrollHeight)) {
			if (content.firstChild && content.firstChild.hasClassName("reduced-display"))
				content.style.outline = "1px solid lightgray";
		}
		if (visualGamelist[i].scrollPos.hasOwnProperty("top")) {
			content.scrollTop = visualGamelist[i].scrollPos.top;
		};
		if (visualGamelist[i].scrollPos.hasOwnProperty("left")) {
			content.scrollLeft = visualGamelist[i].scrollPos.left;
		};
	}
}

function genTable(start, stop, step)
{
	let list="";

	for (var i=start;i<stop;i+=step) {
		const id = visualGamelist[i].id;
		if (id === 0) {
			list += placeholder;
		}
		else {
			list += htmlList[id];
		}
	}
	for (;i<Math.ceil(stop/step)*step;i+=step) {
		list += placeholder;
	}
	list = '<col style="width:75px">'+
	       '<col style="width:53px">'+
	       '<col style="width:150px">'+
	       list;
	return list;
}

function clickTimeout()
{
	if (seconds_reserved)
		seconds_reserved = 0;
	else
		seconds_reserved = defaultTimeout;

	let data = JSON.parse(localStorage.staticGamelist);
	data.timeout = (seconds_reserved>0);
	localStorage.staticGamelist = JSON.stringify(data);
}

function inputColumns(e)
{
	document.getElementById("label_columns").innerText = e.target.value;
	st_columns = parseInt(e.target.value);

	let data = JSON.parse(localStorage.staticGamelist);
	data.columns = st_columns;
	localStorage.staticGamelist = JSON.stringify(data);
}

function clickGuests()
{
	hide_guests = !hide_guests;

	let data = JSON.parse(localStorage.staticGamelist);
	data.hide_guests = hide_guests;
	localStorage.staticGamelist = JSON.stringify(data);
}

var inject_html = document.createElement("script");
inject_html.setAttribute("type", "text/html");
inject_html.id = "_tpl__competition";
inject_html.innerHTML = ''+
'<div class=regular-competition <%if(!params.regular_competition) {%>style="color:black"<%}%>>Соревнование <strong>x<%=(params.regular_competition || 1)%></strong>'+
'<%if(params.competition_cost) {%><div class="cost">стоимость: <%=params.competition_cost%> очков</div><%}%>'+
'</div>';
document.body.appendChild(inject_html);

var inject_css = document.createElement("style");
inject_css.setAttribute("type", "text/css");
inject_css.innerHTML = ''+
'#gamelist .custominfo .regular-competition {'+
'    white-space: nowrap;'+
'}'+
'#gamelist .custominfo .regular-competition .cost {'+
'    color: #777777;'+
'    font-size: 12px;'+
'}'+
'div#gamelist tr.item .status'+
'{'+
	'max-height: 74px;'+
	'overflow: hidden;'+
'}'+
'div#gamelist .imgcont'+
'{'+
	'background-clip: content-box !important;'+
	'padding: 1px;'+
'}'+
'div#gamelist tr.item .players .playerscontainer'+
'{'+
'    height: 71px;'+
'    overflow: auto;'+
'    display: block;'+
'}'+
'.playerscontainer::-webkit-scrollbar'+
'{'+
	'display: none;'+
'}'+
'@supports (-moz-appearance:none) {'+
'    div#gamelist tr.item .players .playerscontainer {'+
'        height: 74px;'+
        'scrollbar-width: none;'+
'    }'+
'}'+
'table[id^="staticTable"]'+
'{'+
	'table-layout:fixed;'+
	'float: left;'+
'}'+
'div#tableContainer:after'+
'{'+
	'content: "";'+
	'display: table;'+
	'clear: both;'+
'}'+
'div#gamelist tr.item .players .playerscontainer table'+
'{'+
	'counter-reset: players;'+
'}'+
'div#gamelist tr.item .players .playerscontainer table div.name'+
'{'+
	'counter-increment: players;'+
'}'+
//https://codepen.io/betty66/pen/wedrxd
'div#gamelist tr.item .players .playerscontainer table.reduced-display::after'+
'{'+
	'content: counter(players);'+
	'background: #00aa00;'+
	'border-radius: 0.8em;'+
	'-moz-border-radius: 0.8em;'+
	'-webkit-border-radius: 0.8em;'+
	'color: #ffffff;'+
	'position:absolute;'+
	'top: 0.8em;'+
	'font-weight: bold;'+
	'line-height: 1.6em;'+
	'text-align: center;'+
	'padding: 0 0.3em 0 0.3em;'+
	'transform: translateX(-100%) translateX(-5px);'+
	'min-width: 1.6em;'+
	'z-index: 1;'+
'}'+
'#anim_speed_table'+
'{'+
	'display: inline-block;'+
	'vertical-align: middle;'+
'}'+
'#reserved_time_table'+
'{'+
	'display: inline-block;'+
	'vertical-align: middle;'+
	'margin: 0 50px 0 50px;'+
'}'+
'#reserved_time_checkbox'+
'{'+
	'margin: 0 0 0 0;'+
'}'+
'label[for=reserved_time_checkbox]'+
'{'+
	'font-weight: normal;'+
	'margin: 0 0 0 5px;'+
'}'+
'#multicolumns_table'+
'{'+
	'display: inline-block;'+
	'vertical-align: middle;'+
'}'+
'#multicolumns_range'+
'{'+
	'width: 90px;'+
	'margin: 0px 10px 0px 10px;'+
'}'+
'label[for=multicolumns_range]'+
'{'+
	'margin: 0 0 0 0;'+
'}'+
'#guests_table'+
'{'+
	'display: inline-block;'+
	'vertical-align: middle;'+
	'margin: 0 50px 0 50px;'+
'}'+
'#guests_checkbox'+
'{'+
	'margin: 0 0 0 0;'+
'}'+
'label[for=guests_checkbox]'+
'{'+
	'font-weight: normal;'+
	'margin: 0 0 0 5px;'+
'}'+
'';
document.body.appendChild(inject_css);

var gamelist_active = jQuery('#gamelist-active');
var gamelist_element = gamelist_active.parent();
var detached = gamelist_active.detach();
gamelist_element.html('<div id="tableContainer"></div>');
gamelist_element.append(detached);

let elem = document.createElement("table");
elem.innerHTML = ''+
	'<tr>'+
	'<td><input type="checkbox" id="reserved_time_checkbox"></td>' +
	'<td><label for="reserved_time_checkbox">Побольше пауза между заездами</label></td>'+
	'</tr>'+
	'';
elem.setAttribute("id", "reserved_time_table");

let elem2 = document.createElement("table");
elem2.innerHTML = ''+
	'<tr>'+
	'<td>Число столбцов:</td>'+
	'<td><input type="range" id="multicolumns_range" min="1" max="6" step="1"></td>'+
	'<td><label id="label_columns" for="multicolumns_range"></label></td>' +
	'</tr>'+
	'';
elem2.setAttribute("id", "multicolumns_table");

let elem3 = document.createElement("table");
elem3.innerHTML = ''+
	'<tr>'+
	'<td><input type="checkbox" id="guests_checkbox"></td>' +
	'<td><label for="guests_checkbox">Фильтр заездов</label></td>'+
	'<td><label id="guests_count" for="guests_checkbox"></label></td>' +
	'</tr>'+
	'';
elem3.setAttribute("id", "guests_table");

let element = jQuery('#anim_speed_table');
element.parent().append(elem);
element.parent().append(elem2);
element.parent().append(elem3);
document.getElementById("reserved_time_checkbox").checked = (seconds_reserved>0);
document.getElementById("reserved_time_checkbox").onclick = clickTimeout;
document.getElementById("multicolumns_range").value = st_columns;
document.getElementById("multicolumns_range").oninput = inputColumns;
document.getElementById("guests_checkbox").checked = hide_guests;
document.getElementById("guests_checkbox").onclick = clickGuests;
inputColumns({target:{value:st_columns}});

setTimeout(function timerListUpdate(){updateList();setTimeout(timerListUpdate,1e3);},1e3);

} //main

var script = document.createElement("script");
script.innerHTML = "(" + main + ")()";
document.body.appendChild(script);
document.body.removeChild(script);
