
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

base = []
with open('kg.tsv') as f:
    lines = f.readlines()
    for line in lines:
        base.append(line.split("\t"))
base = base[1:]

import json

#pos_bg_color = "#ff440055"
#neg_bg_color = "#9293ff55"
#https://filosophy.org/code/online-tool-to-lighten-color-without-alpha-channel/
pos_bg_color = "#FFBEA6" #0.35 opacity
neg_bg_color = "#D9D9FF"
pos_color = "#ff4400"
neg_color = "#9293ff"

import graphviz
dot = graphviz.Digraph()
dot.engine = 'neato'
dot.graph_attr["label"] = r'<<table border="0" cellspacing="0" cellborder="0"><tr><td href="https://klavogonki.ru/forum/general/15833/" title="Конкурс «Репутация — 2021»">Репутация — 2021</td></tr></table>>'
dot.graph_attr["labelloc"] = 't'
dot.graph_attr["fontsize"] = str(72*2.4)
dot.graph_attr["fontname"] = 'Oswald'
dot.graph_attr["fontcolor"] = pos_color
#dot.graph_attr["bgcolor"] = 'black'
dot.graph_attr['overlap'] = 'false'
dot.graph_attr['splines'] = 'true'
dot.graph_attr['sep'] = '+20,40'

dot.node_attr['shape'] = 'box'
dot.node_attr['style'] = 'rounded,filled'
dot.node_attr['fixedsize'] = 'true'
dot.node_attr['penwidth'] = '7.0'
dot.node_attr["fontname"] = 'Roboto Mono'
dot.edge_attr['penwidth'] = '2.0'

rank_colors = {}
rank_colors[1] = "#8d8d8d"
rank_colors[2] = "#4f9a97"
rank_colors[3] = "#187818"
rank_colors[4] = "#8c8100"
rank_colors[5] = "#ba5800"
rank_colors[6] = "#bc0143"
rank_colors[7] = "#5e0b9e"
rank_colors[8] = "#2e32ce"
rank_colors[9] = "#061956"

for row in base:
    _id = row[0]
    _nick = row[1]
    _rep = float(row[2])

    _bgcolor = pos_bg_color if _rep >=0 else neg_bg_color
    _color = pos_color if _rep >=0 else neg_color

    _abs_rep = max(0, abs(_rep))
    _hsize = str(0.5 + _abs_rep/10)
    _area = 1 + _abs_rep
    _letters = max(1, len(_nick)  +2) #+2 - avatar
    _w_to_h_ratio = _letters/3.
    #_h²*_w_to_h_ration = _area
    _h = (_area/_w_to_h_ratio)**0.5
    _w = _h*_w_to_h_ratio
    _h_str = str(_h)
    _w_str = str(_w)
    _fsize_int = _w/_letters*72
    _fsize = str(_fsize_int)

    _votes_plus = int(row[5])
    _votes_minus = int(row[6])
    _votes_total = _votes_plus + _votes_minus

    _img = '<img src="{}/cache/avatars/{}.png" scale="true"/>'.format(script_dir, _id)
    _profile_url = "https://klavogonki.ru/u/#/{}".format(_id)

    _label = ""
    _label += '<<TABLE border="0" cellspacing="0" cellpadding="0" cellborder="0"><tr>'
    _label += '<td colspan="2"><FONT POINT-SIZE="{}">&nbsp;</FONT></td></tr><tr>'.format(_fsize_int//2)
    _label += '<td color="black" border="1" cellpadding="0" cellspacing="0" width="{}" height="{}" fixedsize="true">'.format(_fsize, _fsize) + _img + "</td>"
    _label += '<td href="{}" title="{}">&nbsp;'.format(_profile_url, _nick) + _nick + "</td>"
    _label += '</tr><tr><td colspan="2" style="mystyle"><FONT POINT-SIZE="{}">{}&nbsp;(+{};-{})</FONT></td>'.format(_fsize_int//2, _rep, _votes_plus, _votes_minus)
    _label += "</tr></TABLE>>"

    if _votes_total == 0:
        _fillcolor = "white"
    else:
        if _votes_plus>0 and _votes_minus>0:
            _fillcolor = pos_bg_color + ";" + str(_votes_plus/_votes_total) + ":" + neg_bg_color
        elif _votes_plus>0:
            _fillcolor = pos_bg_color
        else:
            _fillcolor = neg_bg_color

    _lfile = open("cache/summary/"+_id, "r")
    _summary = json.loads(_lfile.read())
    _lfile.close()

    try:
        _level = _summary['level']
    except:
        print(_id)
        print(_summary)
        _level = 1
    _fontcolor = rank_colors[int(_level)]

    dot.node(str(_nick), **{'fontsize': _fsize, 'height': _h_str, 'width': _w_str, 'fillcolor': _fillcolor, 'color': _color, 'label': _label, 'fontcolor': _fontcolor})

for row in base:
    _id = row[0]
    _nick = row[1]
    v = json.loads(row[7])
    for _v in v:
        _color = pos_color if _v[2]>=0 else neg_color
        dot.edge(str(_nick), str(_v[1]), constraint='false', **{'color': _color})

_svg =  dot._repr_svg_()

import re
import base64

# embed avatars into svg

def png_repl(matchobj):
    _file = matchobj.group(1)+".png"
    encoded_string = ""
    with open(_file, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
    return "<image href=\"data:image/png;base64,{}\" width".format(encoded_string)

_svg = re.sub("<image xlink:href=\"(.*?)\.png\" width", png_repl, _svg)

# embed fonts into svg

def encode_font(_file):
    encoded_string = ""
    with open(_file, "rb") as font_file:
        encoded_string = base64.b64encode(font_file.read()).decode('utf-8')
    return "data:application/font-truetype;charset=utf-8;base64,{}".format(encoded_string)

font1_str = '<defs><style>@font-face {font-family: \"Oswald\";src: url(\"' + encode_font("fonts/Oswald-Regular.ttf") + '\");}</style></defs>'

_css = """
#checkbox_label {
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    margin: 0 20px;
    color: {};
}
g.node > text {
    display: none;
}
.checkmark {
    display: none;
    pointer-events: none;
}
""".replace("{}", pos_color)
css_str = '<defs><style>{}</style></defs>'.format(_css)


font2_str = '<defs><style>@font-face {font-family: \"Roboto Mono\";src: url(\"' + encode_font("fonts/RobotoMono-Regular.ttf") + '\");}</style></defs>'

def font_repl(matchobj):
    return matchobj.group(0)+"\n"+font1_str+"\n"+css_str+"\n"

def font_repl2(matchobj):
    return matchobj.group(0)+"\n"+font2_str

_svg = re.sub("<svg(.*?)>", font_repl2, _svg, flags=re.DOTALL)
_svg = re.sub("<svg(.*?)>", font_repl, _svg ,flags=re.DOTALL)


_file = open("checkmark.svg","r")
_chbx_svg = _file.read()
_file.close()
_legend = """
    <svg viewBox="0 0 640 200" width="100%" height="200px"
        preserveAspectRatio="xMaxYMin meet">

        {checkbox}
    <text xmlns="http://www.w3.org/2000/svg" id="checkbox_label" x="640" y="100" text-anchor="end" font-family="Oswald" font-size="60" fill="#ff4400">Показывать результаты</text>
    </svg>
  <script>
  // <![CDATA[
  window.addEventListener('DOMContentLoaded', () => {
      function toggle() {
          document.querySelectorAll('g.checkmark').forEach(el => {el.style.display = (el.style.display !== 'block') ? 'block' : 'none'});
          document.querySelectorAll('g.node > text').forEach(el => {el.style.display = (el.style.display !== 'block') ? 'block' : 'none'});
      }
    document.querySelector('#checkbox_label').addEventListener('click', (e) => {
        toggle();
    })
    document.querySelector('#checkbox_box').addEventListener('click', (e) => {
        toggle();
    })
  })
  // ]]>
  </script>

</svg>
"""

_legend = _legend.replace("{checkbox}", _chbx_svg)
_legend = _legend.replace("{color}", pos_color)
_legend = _legend.replace("{neg_color}", neg_color)

start = """
<svg width="1075" height="1010" viewBox="0 0 1075 1010" xmlns="http://www.w3.org/2000/svg">

<defs>
    <pattern id="arrows" x="0" y="0" width="50" height="20" patternUnits="userSpaceOnUse">
    <g transform="scale(0.078)">
        <polygon points="0,140 140,0 280,140 210,140 210,250 70,250 70,140 0,140" fill="{color}" />
        <polygon points="320,110 460,250 600,110 530,110 530,0 390,0 390,110 320,110" fill="{neg_color}" />
    </g>
    </pattern>
    <pattern id="arrows_vert" x="0" y="0" width="25" height="45" patternUnits="userSpaceOnUse">
    <g transform="scale(0.078)">
        <polygon points="0,140 140,0 280,140 210,140 210,250 70,250 70,140 0,140" fill="{color}" />
        <polygon points="0,400 140,540 280,400 210,400 210,290 70,290 70,400 0,400" fill="{neg_color}" />
    </g>
    </pattern>
</defs>
    <rect x="0" y="0" width="1075" height="1010" fill="white" />
    <rect x="0" y="0" width="1075" height="20" fill="url(#arrows)" />
    <rect x="0" y="0" width="25" height="1010" fill="url(#arrows_vert)" />
    <rect x="0" y="0" width="1075" height="20" fill="url(#arrows)" transform="translate(0 990)"/>
    <rect x="1050" y="0" width="25" height="1010" fill="url(#arrows_vert)" />

<svg width="1000" height="1000" x="32.5" y="5" preserveAspectRatio="xMidYMid meet"
"""
start = start.replace("{color}", pos_color)
start = start.replace("{neg_color}", neg_color)
_svg = re.sub("<svg width=\"(.*?)\" height=\"(.*?)\"", start, _svg, flags=re.DOTALL)

def legend_repl(matchobj):
    return "\n"+_legend+"\n"+matchobj.group(0)
_svg = re.sub("</svg>", legend_repl, _svg, flags=re.DOTALL)

_filetest = "out.svg"
_f = open(_filetest, "w")
_f.write(_svg)
_f.close()

