
avatars_dir = "cache/avatars/"

import pathlib
pathlib.Path(avatars_dir).mkdir(parents=True, exist_ok=True) 

import urllib.request
import os.path

base = []
with open('kg.tsv') as f:
    lines = f.readlines()
    for line in lines:
        base.append(line.split("\t"))
base =  base[1:]

import time
for _b in base:
    _id = _b[0]
    _url = "https://klavogonki.ru/storage/avatars/{}_big.png".format(_id)
    _filename = avatars_dir+_id+".png"
    if not os.path.isfile(_filename):
        try:
            print(_id)
            f = urllib.request.urlopen(_url)
            time.sleep(1)
        except:
            print("except")
            f = open("avatar_dummy.png", "rb")
        _f = open(_filename,"wb")
        _f.write(f.read())
        _f.close()
        f.close()

