
summary_dir = "cache/summary/"

import pathlib
pathlib.Path(summary_dir).mkdir(parents=True, exist_ok=True) 

import urllib.request
import os.path

base = []
with open('kg.tsv') as f:
    lines = f.readlines()
    for line in lines:
        base.append(line.split("\t"))
base =  base[1:]

import time
for _b in base:
    _id = _b[0]
    _url = r"https://klavogonki.ru/api/profile/get-summary?id={}".format(_id)
    _filename = summary_dir+_id
    if not os.path.isfile(_filename):
        try:
            print(_id)
            f = urllib.request.urlopen(_url)
            time.sleep(1)
            _f = open(_filename, "wb")
            _f.write(f.read())
            _f.close()
            f.close()
        except:
            print("except")

